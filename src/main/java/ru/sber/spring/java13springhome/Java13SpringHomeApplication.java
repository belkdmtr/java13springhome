package ru.sber.spring.java13springhome;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Java13SpringHomeApplication {

    public static void main(String[] args) {
        SpringApplication.run(Java13SpringHomeApplication.class, args);
    }

}
