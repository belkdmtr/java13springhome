package ru.sber.spring.java13springhome.library.repository;

import ru.sber.spring.java13springhome.library.model.Film;
import ru.sber.spring.java13springhome.library.model.Order;

public interface OrderRepository
        extends GenericRepository<Order> {
}
