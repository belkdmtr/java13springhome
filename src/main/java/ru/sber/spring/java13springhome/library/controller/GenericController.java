package ru.sber.spring.java13springhome.library.controller;

import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.sber.spring.java13springhome.library.model.GenericModel;
import ru.sber.spring.java13springhome.library.repository.GenericRepository;

import java.time.LocalDateTime;
import java.util.List;


/*GenericController унаследовали от GenericModel, что бы ни какой класс кроме наших классов
Order/Book/Director.... унаследованных так же GenericModel больше ничего не могло попасть сюда
*/
@RestController
public abstract class GenericController<T extends GenericModel> {

    private final GenericRepository<T> genericRepository;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection") // Что бы не светилась красным genericRepository
    public GenericController(GenericRepository<T> genericRepository) {
        this.genericRepository = genericRepository;
    }

    // Получить данные о сущности по его ID
    @Operation(description = "Получить информацию по ID", method = "getById")
    // URI "/getById", тип транспорта "GET" и формат данных возращаемый при вызове метода - APPLICATION_JSON_VALUE
    //и на вход подается id фильма - @RequestParam(value = "id"
    @RequestMapping(value = "/getById", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<T> getFilmId(@RequestParam(value = "id") Long id) {

        //1й) способ получения данных из таблицы.
        return ResponseEntity.status(HttpStatus.OK).body(genericRepository
                .findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Нет данных по ID = " + id)));
    }

    //Получить все записи
    @Operation(description = "Получить все записи", method = "getAll")
    @RequestMapping(value = "/getAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<T>> getAll() {
        return ResponseEntity.status(HttpStatus.OK).body(genericRepository.findAll());
    }

    // Создать сущность
    @Operation(description = "Создать сущность", method = "create")
    @RequestMapping(value = "/create", method = RequestMethod.POST,
                    produces = MediaType.APPLICATION_JSON_VALUE, // возвращает APPLICATION_JSON_VALUE
                    consumes = MediaType.APPLICATION_JSON_VALUE) // принимает APPLICATION_JSON_VALUE (в теле запроса будем посылать инфу по нашему фильму)
    public ResponseEntity<T> create(@RequestBody T newEntity) {
        newEntity.setCreatedWhen(LocalDateTime.now()); // время создания записывается в момент записи
        return ResponseEntity.status(HttpStatus.CREATED).body(genericRepository.save(newEntity));
    }

    // Обновить Сущность
    @Operation(description = "Обновить сущность", method = "update")
    @RequestMapping(value = "/update", method = RequestMethod.PUT,
                    produces = MediaType.APPLICATION_JSON_VALUE,
                    consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<T> update(@RequestBody T newEntity,
                                    @RequestParam(name = "id") Long id) { //так как обновляем сущность, должны запросить ID
        newEntity.setId(id); //Если ID будет, то обновятся поля или создастся новая запись
        return ResponseEntity.status(HttpStatus.CREATED).body(genericRepository.save(newEntity));
    }

    // Удалить Сущность
    @Operation(description = "Удалить сущность", method = "delete")
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    void delete (@RequestParam(value = "id") Long id) {
        genericRepository.deleteById(id); //Удаляет запись, а если ее нет просто тихо скажет, что удалили без ошибок
    }
}
