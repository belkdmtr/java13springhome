package ru.sber.spring.java13springhome.library.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Entity
@Table(name = "directors")
@Getter
@Setter
@NoArgsConstructor
@SequenceGenerator(name = "default_generator", sequenceName = "directors_seq", allocationSize = 1)
@JsonIdentityInfo(generator = ObjectIdGenerators.UUIDGenerator.class, property = "@json_id")
public class Director
        extends GenericModel {

    @Column(name = "director_fio", nullable = false)
    private String directorFio;

    @Column(name = "position")
    private String position;

    //СВЯЗЬ МЭНИ-ТО-МЭНИ для второстепенной таблице
//    @ManyToMany(mappedBy = "directors") //Указали название поля из класса Films *private Set<Directors> directors*
//    private Set<Film> films;

    //Делаем равнозначными Фильмы и Режиссеров
    @ManyToMany
    @JoinTable(name = "films_directors",
                joinColumns = @JoinColumn(name = "director_id"), foreignKey = @ForeignKey(name = "FK_DIRECTORS_FILMS"),
                inverseJoinColumns = @JoinColumn(name = "film_id"), inverseForeignKey = @ForeignKey(name = "FK_FILMS_DIRECTORS"))
    private Set<Film> films;

}
