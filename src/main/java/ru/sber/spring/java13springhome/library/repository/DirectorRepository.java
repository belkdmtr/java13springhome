package ru.sber.spring.java13springhome.library.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.sber.spring.java13springhome.library.model.Director;

@Repository
public interface DirectorRepository
        extends GenericRepository<Director> {
}

/*
@Repository
public interface DirectorRepository
        extends JpaRepository<Director, Long> {
}
 */
