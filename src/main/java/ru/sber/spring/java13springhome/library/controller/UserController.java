package ru.sber.spring.java13springhome.library.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.webjars.NotFoundException;
import ru.sber.spring.java13springhome.library.model.Role;
import ru.sber.spring.java13springhome.library.model.User;
import ru.sber.spring.java13springhome.library.repository.RoleRepository;
import ru.sber.spring.java13springhome.library.repository.UserRepository;

import java.util.Set;


@RestController
@RequestMapping(value = "/users")
@Tag(name = "Пользователи",
        description = "Контроллер для работы с пользователями")
public class UserController
        extends GenericController<User>{

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    public UserController(UserRepository userRepository, RoleRepository roleRepository) {
        super(userRepository);
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    // Добавим к Юзеру роль
    @Operation(description = "Добавить Юзеру его роль", method = "addRole")
    @RequestMapping(value = "/addRole", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> addDirector(@RequestParam(value = "userId") Long userId,
                                            @RequestParam(value = "roleId") Long roleId) {

        User user = userRepository.findById(userId).orElseThrow(() -> new NotFoundException("Юзер не найден"));
        Role role = roleRepository.findById(roleId).orElseThrow(() -> new NotFoundException("Роль не найдена"));

        // К Юзеру добавим роль
        user.setRoles(role);

        return ResponseEntity.status(HttpStatus.CREATED).body(userRepository.save(user));
    }

}
