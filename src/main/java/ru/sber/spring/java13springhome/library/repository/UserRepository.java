package ru.sber.spring.java13springhome.library.repository;

import ru.sber.spring.java13springhome.library.model.Order;
import ru.sber.spring.java13springhome.library.model.User;

public interface UserRepository
        extends GenericRepository<User>{
}
