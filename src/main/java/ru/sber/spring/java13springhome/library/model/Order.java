package ru.sber.spring.java13springhome.library.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Table(name = "orders")
@Getter
@Setter
@NoArgsConstructor
@SequenceGenerator(name = "default_generator", sequenceName = "orders_seq", allocationSize = 1)
@JsonIdentityInfo(generator = ObjectIdGenerators.UUIDGenerator.class, property = "@json_id")
public class Order
        extends GenericModel {

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = true, foreignKey = @ForeignKey(name = "FK_ORDERS_USERS"))
    private User users;

    @ManyToOne
    @JoinColumn(name = "film_id", nullable = true, foreignKey = @ForeignKey(name = "FK_ORDERS_FILMS"))
    private Film films;

    @Column(name = "rent_date")
    private LocalDate rentDate;

    //поле автоматичесмки расчитывается из rent_date + rent_period
    @Column(name = "rent_period")
    private Integer rentPeriod;

    //Была ли продана книга окончательно
    @Column(name = "purchase")
    private boolean purchase;

}