package ru.sber.spring.java13springhome.library.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.sber.spring.java13springhome.library.model.Film;

@Repository
public interface FilmRepository
        extends GenericRepository<Film> { //С каким классом/моделью мы работаем -Film и с каким типом данных - Long
}

/*
@Repository
public interface FilmRepository
        extends JpaRepository<Film, Long> { //С каким классом/моделью мы работаем -Film и с каким типом данных - Long
}
 */
