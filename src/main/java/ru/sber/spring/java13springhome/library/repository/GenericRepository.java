package ru.sber.spring.java13springhome.library.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ru.sber.spring.java13springhome.library.model.GenericModel;

/**
 * Абстрактный репозиторий
 * Необходим для работы абстрактного сервиса,
 * т.к. в абстрактном сервисе мы не можем использовать конкретный репозиторий,
 * а должны указывать параметризованный (GenericRepository)
 * @param <T> - Сущность с которой работает репозиторий
 */

@NoRepositoryBean // По аналогии с GenericModel помечаем его антоцией, что бы бин репозитория не создавался
public interface GenericRepository<T extends GenericModel>
        extends JpaRepository<T, Long> {
}
