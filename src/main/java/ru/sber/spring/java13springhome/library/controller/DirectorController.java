package ru.sber.spring.java13springhome.library.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.webjars.NotFoundException;
import ru.sber.spring.java13springhome.library.model.Director;
import ru.sber.spring.java13springhome.library.model.Film;
import ru.sber.spring.java13springhome.library.repository.DirectorRepository;
import ru.sber.spring.java13springhome.library.repository.FilmRepository;

@RestController
@RequestMapping(value = "/directors")
@Tag(name = "Режиcсеры", description = "Контроллер для работы с режиссерами")
public class DirectorController
        extends GenericController<Director>{

    private final DirectorRepository directorRepository;
    private final FilmRepository filmRepository;

    public DirectorController(DirectorRepository directorRepository,
                              FilmRepository filmRepository) {
        super(directorRepository);
        this.directorRepository = directorRepository;
        this.filmRepository = filmRepository;
    }


    // Добавим к режиссеру фильм
    @Operation(description = "Добавить к режиссеру фильм", method = "addFilm")
    @RequestMapping(value = "/addFilm", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Director> addDirector(@RequestParam(value = "filmId") Long filmId,
                                            @RequestParam(value = "directorId") Long directorId) {

        Film film = filmRepository.findById(filmId).orElseThrow(() -> new NotFoundException("Фильм не найден"));
        Director director = directorRepository.findById(directorId).orElseThrow(() -> new NotFoundException("Режиссер не найден"));

        // Добавим директора к фильму
        director.getFilms().add(film); //К существующей коллекции режиссеров у фильма добавим нового режиссера

        return ResponseEntity.status(HttpStatus.CREATED).body(directorRepository.save(director));
    }
/*
    // Получить данные о режисере по его ID
    @Operation(description = "Получить информацию по ID", method = "getById")
    @RequestMapping(value = "/getById", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Director> getFilmId(@RequestParam(value = "id") Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(directorRepository
                .findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Нет данных по ID = " + id)));
    }

    // Создать Режисера
    @Operation(description = "Добавить Автора", method = "create")
    @RequestMapping(value = "/create", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Director> create(@RequestBody Director newDirector) {
        return ResponseEntity.status(HttpStatus.CREATED).body(directorRepository.save(newDirector));
    }

    // Обновить Режисера
    @Operation(description = "Обновить режисера", method = "update")
    @RequestMapping(value = "/update", method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Director> update(@RequestBody Director updateDirector,
                                       @RequestParam(name = "id") Long id) { //так как обновляем книгу, должны запросить ID
        updateDirector.setId(id); //Если ID будет, то обновятся поля или создастся новая запись
        return ResponseEntity.status(HttpStatus.CREATED).body(directorRepository.save(updateDirector));
    }

    // Удалить Режисера
    @Operation(description = "Удалить режисера", method = "delete")
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    void delete (@RequestParam(value = "id") Long id) {
        directorRepository.deleteById(id); //Удаляет запись, а если ее нет просто тихо скажет, что удалили без ошибок

    }
*/

}
