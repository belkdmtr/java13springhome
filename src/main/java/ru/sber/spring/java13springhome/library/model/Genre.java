package ru.sber.spring.java13springhome.library.model;

public enum Genre {

    FANTASY("Фантастика"),
    DRAMA("Драма"),
    DOCUMENTARY("Документальное"),
    HORROR("Ужасы"),
    ACTION("Боевик");

    private final String genreTextDisplay;

    Genre(String text){
        this.genreTextDisplay = text;
    }

    public String getGenreTextDisplay(){
        return this.genreTextDisplay;
    }

}
