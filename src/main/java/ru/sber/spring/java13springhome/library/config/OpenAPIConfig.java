package ru.sber.spring.java13springhome.library.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenAPIConfig {
    //http://localhost:9090/api/rest/swagger-ui/index.html
    @Bean
    public OpenAPI filmographyProject() {
        return new OpenAPI()
                .info(new Info()
                        .title("Фильмотека")
                        .description("Сервис позволяющий взять на прокат фильм")
                        .version("v0.1")
                        .license(new License().name("Apache 2.0").url("http://springdoc.org"))
                        .contact(new Contact().name("Dmitrii Belkin")
                                .email("aka_diamond@mail.ru")
                                .url(""))
                );
    }
}
