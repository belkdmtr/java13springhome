package ru.sber.spring.java13springhome.library.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@MappedSuperclass //помечаем аннотацией, чтобы класс не создавался в таблице БД
public class GenericModel {

    @Id //помечает, что является первичным ключом
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "default_generator")
    @Column(name = "id", nullable = false) //nullable = false не может быть пустым
    private Long id;

    //когда создано
    @Column(name = "created_when", nullable = false)
    private LocalDateTime createdWhen;

//    //кем создано
//    @Column(name = "created_by", nullable = false)
//    private String createdBy;

}
