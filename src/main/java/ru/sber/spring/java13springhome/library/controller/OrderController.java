package ru.sber.spring.java13springhome.library.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.webjars.NotFoundException;
import ru.sber.spring.java13springhome.library.model.Film;
import ru.sber.spring.java13springhome.library.model.Order;
import ru.sber.spring.java13springhome.library.model.User;
import ru.sber.spring.java13springhome.library.repository.FilmRepository;
import ru.sber.spring.java13springhome.library.repository.OrderRepository;
import ru.sber.spring.java13springhome.library.repository.UserRepository;

@RestController
@RequestMapping(value = "/orders")
@Tag(name = "Заказы", description = "Контроллер для работы с заказами")
public class OrderController extends
        GenericController <Order>{

    private final OrderRepository orderRepository;
    private final FilmRepository filmRepository;
    private final UserRepository userRepository;

    public OrderController (OrderRepository orderRepository,
                            FilmRepository filmRepository,
                            UserRepository userRepository) {
        super(orderRepository);
        this.orderRepository = orderRepository;
        this.filmRepository = filmRepository;
        this.userRepository = userRepository;
    }

    // Добавим к заказу фильм
    @Operation(description = "Добавить к заказу фильм", method = "addFilm")
    @RequestMapping(value = "/addFilm", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Order> addFilm(@RequestParam(value = "orderId") Long orderId,
                                         @RequestParam(value = "filmId") Long filmId) {
        Order order = orderRepository.findById(orderId).orElseThrow(() -> new NotFoundException("Заказ не найден не найден"));
        Film film = filmRepository.findById(filmId).orElseThrow(() -> new NotFoundException("Фильм не найден"));

        // К Фильму добавим режиссера
        order.setFilms(film);

        // Сохраним изменения
        return ResponseEntity.status(HttpStatus.CREATED).body(orderRepository.save(order));
    }


    // Добавим к заказу пользователя
    @Operation(description = "Добавить к заказу пользователя", method = "addUser")
    @RequestMapping(value = "/addUser", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Order> addUser(@RequestParam(value = "orderId") Long orderId,
                                         @RequestParam(value = "userId") Long userId) {
        Order order = orderRepository.findById(orderId).orElseThrow(() -> new NotFoundException("Заказ не найден не найден"));
        User user = userRepository.findById(userId).orElseThrow(() -> new NotFoundException("Пользователь не найден"));

        // К Фильму добавим режиссера
        order.setUsers(user);

        // Сохраним изменения
        return ResponseEntity.status(HttpStatus.CREATED).body(orderRepository.save(order));
    }

}
