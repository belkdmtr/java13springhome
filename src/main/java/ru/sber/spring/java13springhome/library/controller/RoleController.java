package ru.sber.spring.java13springhome.library.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.sber.spring.java13springhome.library.model.Role;
import ru.sber.spring.java13springhome.library.repository.RoleRepository;

@RestController
@RequestMapping(value = "/roles")
@Tag(name = "Роли пользователей",
        description = "Контроллер для работы с ролями пользователей")
//localhost:9090/api/rest/films
public class RoleController
        extends GenericController<Role> {

    private final RoleRepository roleRepository;

    public RoleController(RoleRepository roleRepository){
        super(roleRepository);
        this.roleRepository = roleRepository;
    }
}
