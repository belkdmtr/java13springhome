package ru.sber.spring.java13springhome.library.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Set;

@Entity //Сущность (хибернейт)
@Table(name = "films") //класс Film будет явно мапиться на таблицу films (если нет то создаст таблицу)
@Getter
@Setter
@NoArgsConstructor //для сериализации
@SequenceGenerator(name = "default_generator", sequenceName = "films_seq", allocationSize = 1)
@JsonIdentityInfo(generator = ObjectIdGenerators.UUIDGenerator.class, property = "@json_id")
public class Film
        extends GenericModel {

    //Описание
    @Column(name = "title", nullable = false)
    private String title;

    //Дата премьеры
    @Column(name = "premier_year", nullable = false)
    private LocalDate premierYear;

    //Страна происхождения
    @Column(name = "country")
    private String country;

    //Жанр
    @Column(name = "genre", nullable = false)
    @Enumerated //Помечаем анатацией, что жанр в БД будет храниться цифрами из нашего enum Genre
    private Genre genre;

    //СОЗДАНИЕ ВСПОМОГАТЕЛЬНОЙ ТАБЛИЦЫ МЭНИ-ТО-МЭНИ
    @ManyToMany
    //@JoinTable - надстройка в главной таблице при помощи которой мы реализуем связь мэни-томэни
    @JoinTable(name = "films_directors",
            joinColumns = @JoinColumn(name = "film_id"), foreignKey = @ForeignKey(name = "FK_FILMS_DIRECTORS"),
            inverseJoinColumns = @JoinColumn(name = "director_id"), inverseForeignKey = @ForeignKey(name = "FK_DIRECTORS_FILMS"))
    private Set<Director> directors;

    @OneToMany(mappedBy = "films")
    private Set<Order> orders;

}
