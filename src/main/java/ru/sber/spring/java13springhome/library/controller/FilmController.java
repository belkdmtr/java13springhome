package ru.sber.spring.java13springhome.library.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.webjars.NotFoundException;
import ru.sber.spring.java13springhome.library.model.Director;
import ru.sber.spring.java13springhome.library.model.Film;
import ru.sber.spring.java13springhome.library.repository.DirectorRepository;
import ru.sber.spring.java13springhome.library.repository.FilmRepository;


@RestController
@RequestMapping(value = "/films")
@Tag(name = "Фильмы",
        description = "Контроллер для работы с фильмами фильмотеки")
//localhost:9090/api/rest/films
public class FilmController
        extends GenericController<Film> {

    private final FilmRepository filmRepository;

    private final DirectorRepository directorRepository;

    public FilmController(FilmRepository filmRepository, DirectorRepository directorRepository) {
        super(filmRepository); // Вызвать конструктор super, который будет работать для репозитория фильмов
        this.filmRepository = filmRepository;
        this.directorRepository = directorRepository;
    }

    // Добавим к фильму режиссера
    @Operation(description = "Добавить режиссера к фильму", method = "addDirector")
    @RequestMapping(value = "/addDirector", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Film> addDirector(@RequestParam(value = "filmId") Long filmId,
                                            @RequestParam(value = "directorId") Long directorId) {

        Film film = filmRepository.findById(filmId).orElseThrow(() -> new NotFoundException("Фильм не найден"));
        Director director = directorRepository.findById(directorId).orElseThrow(() -> new NotFoundException("Режиссер не найден"));

        // К Фильму добавим режиссера
        film.getDirectors().add(director); //К существующей коллекции режиссеров у фильма добавим нового режиссера

        return ResponseEntity.status(HttpStatus.CREATED).body(filmRepository.save(film));

    }



/*
    // Получить данные о фильме по его ID
    @Operation(description = "Получить информацию по ID", method = "getById")
    // URI "/getById", тип транспорта "GET" и формат данных возращаемый при вызове метода - APPLICATION_JSON_VALUE
    //и на вход подается id фильма - @RequestParam(value = "id"
    @RequestMapping(value = "/getById", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Film> getFilmId(@RequestParam(value = "id") Long id) {

        //1й) способ получения данных из таблицы.
        return ResponseEntity.status(HttpStatus.OK).body(filmRepository
                                                            .findById(id)
                                                            .orElseThrow(() -> new IllegalArgumentException("Нет данных по ID = " + id)));
    }

    // Создать фильм
    @Operation(description = "Создать фильм", method = "create")
    @RequestMapping(value = "/create", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE, // возвращает APPLICATION_JSON_VALUE
            consumes = MediaType.APPLICATION_JSON_VALUE) // принимает APPLICATION_JSON_VALUE (в теле запроса будем посылать инфу по нашему фильму)
    public ResponseEntity<Film> create(@RequestBody Film newFilm) {
        return ResponseEntity.status(HttpStatus.CREATED).body(filmRepository.save(newFilm));
    }

    // Обновить Фильм
    @Operation(description = "Обновить фильм", method = "update")
    @RequestMapping(value = "/update", method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Film> update(@RequestBody Film updateFilm,
                                       @RequestParam(name = "id") Long id) { //так как обновляем книгу, должны запросить ID
        updateFilm.setId(id); //Если ID будет, то обновятся поля или создастся новая запись
        return ResponseEntity.status(HttpStatus.CREATED).body(filmRepository.save(updateFilm));
    }

    // Удалить Фильм
    @Operation(description = "Удалить фильм", method = "delete")
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    void delete (@RequestParam(value = "id") Long id) {
        filmRepository.deleteById(id); //Удаляет запись, а если ее нет просто тихо скажет, что удалили без ошибок
    }
*/
}
