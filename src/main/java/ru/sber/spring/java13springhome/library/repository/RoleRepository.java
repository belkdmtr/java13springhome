package ru.sber.spring.java13springhome.library.repository;

import ru.sber.spring.java13springhome.library.model.Director;
import ru.sber.spring.java13springhome.library.model.Role;

public interface RoleRepository
        extends GenericRepository<Role>{
}
